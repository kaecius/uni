package org.pmoo.euskovision;

public class EuskoVision {
    private ListaBertsolaris lista;
    private double dineroSinGastar;
    private static  EuskoVision miEuskoVision = new EuskoVision();

    private EuskoVision(){
        this.lista = new ListaBertsolaris();
        this.dineroSinGastar = 100000.0;
    }

    public static EuskoVision getEuskoVision(){
        return miEuskoVision;
    }

    public int numBertsolarisSinCosteAutopista(){
        return this.lista.numBertsolarisCosteCero();
    }

    public void imprimerPrimerBertsolariComarca(String pComarca){
        this.lista.primerParticipante(pComarca);
    }

    public ListaBertsolaris getLista() {
        return lista;
    }

    public void setLista(ListaBertsolaris lista) {
        this.lista = lista;
    }

    public double getDineroSinGastar() {
        return dineroSinGastar;
    }

    public void setDineroSinGastar(double dineroSinGastar) {
        this.dineroSinGastar = dineroSinGastar;
    }
}
