package org.pmoo.euskovision;

import java.util.ArrayList;
import java.util.Iterator;

public class ListaBertsolaris {

    private ArrayList<Bertsolari> bertsolaris;

    public ListaBertsolaris(){
        this.bertsolaris = new ArrayList<>();
    }
    
    public int numBertsolarisCosteCero() {
        int cont = 0;

        Iterator<Bertsolari> iterator = this.bertsolaris.iterator();

        while(iterator.hasNext()){
            Bertsolari b = iterator.next();
            if (b.getCosteAutopista() == 0){
                cont++;
            }
        }

        return cont;
    }

    public void primerParticipante(String pComarca){
        Bertsolari primer = null;
        boolean encontrado = false;
        Iterator<Bertsolari> iterator = this.bertsolaris.iterator();
        while(iterator.hasNext() && !encontrado){
            primer = iterator.next();
            if (primer.getComarca().equals(pComarca)){
                encontrado = true;
            }
        }
        if (encontrado){
            System.out.println(primer.getComarca());
        }else{
            System.out.println("No hay");
        }
    }
}
