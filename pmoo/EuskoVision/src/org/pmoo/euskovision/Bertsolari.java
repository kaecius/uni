package org.pmoo.euskovision;

public class Bertsolari {
    private String nombreCompleto;
    private String comarca;
    private String tituloCancion;
    private double costeAutopista;

    public Bertsolari(String pNombreCompleto, String pComarca, String pTituloCancion, double costeAutopista){
        this.nombreCompleto = pNombreCompleto;
        this.comarca = pComarca;
        this.tituloCancion = pTituloCancion;
        this.costeAutopista = costeAutopista;
    }

    public double getCosteAutopista(){
        return this.costeAutopista;
    }

    public String getComarca(){
        return this.comarca;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public void setComarca(String comarca) {
        this.comarca = comarca;
    }

    public String getTituloCancion() {
        return tituloCancion;
    }

    public void setTituloCancion(String tituloCancion) {
        this.tituloCancion = tituloCancion;
    }

    public void setCosteAutopista(double costeAutopista) {
        this.costeAutopista = costeAutopista;
    }
}
