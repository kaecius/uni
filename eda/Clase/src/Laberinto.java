import java.util.Stack;

public class Laberinto {

    private class Casilla{
        private int f,c;
    }


    public boolean hayCamino(){
        Stack<Casilla> porExaminar = new Stack<>();
        porExaminar.push(new Casilla(0,0));
        Boolean[][] visitados = new Boolean[laberinto.length][laberinto[0].length];
        visitados[0][0] = true;
        Casilla destino = new Casilla(laberinto.length -1 , laberinto[0].length -1);
        boolean enc = false;
        while (!enc && !porExaminar.isEmpty()) {
            Casilla act = porExaminar.pop();
            if (act.equals(destino)){
                enc = true;
            }else{
                int i = act.f;
                int j = act.c+1;
                if (dentro(i,j) && laberinto[i][j] != 0 && !visitados[i][j]){
                    porExaminar.push(new Casilla(i,j));
                    visitados[i][j] = true;
                }
            }
        }
        if (enc)
    }

}
