package eus.ehu.ikasle.eda;

import java.util.ArrayList;

public class Camino {

    private Node first;

    public CircularLinkedList<String> obtenerCoordenadas(ArrayList<String> acciones){
        // O(n) n = numero de acciones
        CircularLinkedList<String> result = new CircularLinkedList<>();
        Node actual = this.first;
        result.addToRear(actual.getCoord());
        for(String accion : acciones){
            if (accion.equalsIgnoreCase("adelante")){
                actual = actual.getNext();
            }else if (accion.equalsIgnoreCase("izquierda")){
                actual = actual.getLeft();
            }else if(accion.equalsIgnoreCase("derecha")){
                actual = actual.getRight();
            }
            result.addToRear(actual.getCoord());
        }

        return result;
    }

}
