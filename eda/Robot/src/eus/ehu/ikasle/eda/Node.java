package eus.ehu.ikasle.eda;

public class Node {
    private String coord;
    private Node next;
    private Node left;
    private Node right;

    public Node(String coord) {
        this.coord = coord;
    }

    public String getCoord() {
        return coord;
    }

    public Node getNext() {
        return next;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }

}
