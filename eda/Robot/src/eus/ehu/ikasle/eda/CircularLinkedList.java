package eus.ehu.ikasle.eda;

public class CircularLinkedList<T extends Comparable<T>> {

    private NodeCircularLinkedList<T> last;

    public void addToRear(T data) {
        NodeCircularLinkedList<T> node = new NodeCircularLinkedList<T>(data);
        if (this.last != null){
            node.setNext(this.last.getNext());
            this.last.setNext(node);
            this.last = node;

        }else{
            this.last = node;
            this.last.setNext(node);

        }
    }

    public void printLista() {
        NodeCircularLinkedList<T> actual = this.last.getNext();
        do {
            System.out.println(actual.getData());
            actual = actual.getNext();
        }while(!actual.equals(this.last));
    }
}
