package eus.ehu.ikasle.eda;

public class NodeCircularLinkedList<T extends Comparable<T>> {

    private T data;
    private NodeCircularLinkedList<T> next;


    public NodeCircularLinkedList(T data) {
        this.data = data;
    }

    public NodeCircularLinkedList(T data, NodeCircularLinkedList<T> next) {
        this.data = data;
        this.next = next;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public NodeCircularLinkedList<T> getNext() {
        return next;
    }

    public void setNext(NodeCircularLinkedList<T> next) {
        this.next = next;
    }
}
