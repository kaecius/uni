package eus.ehu.ikasle.eda;

public class Matriz {

    private Node first;


    public Matriz suma(Matriz m1, Matriz m2){
        Matriz resultado = new Matriz();
        Node lastInsertion = resultado.first;
        Node actM1 = m1.first;
        Node actM2 = m2.first;

        while(actM1 != null || actM2 != null){
            if (actM1 != null){
                if (actM2 != null){
                    if (actM1.getFila() == actM2.getFila()){
                        if (actM1.getColumna() == actM2.getColumna()){
                            Node tmp = new Node(actM1.getDato()+actM2.getDato(),
                                    actM1.getFila(),
                                    actM1.getColumna());
                            lastInsertion = addNode(resultado, lastInsertion, tmp);
                            actM1 = actM1.getNext();
                            actM2 = actM2.getNext();
                        }else if (actM1.getColumna() < actM2.getColumna()){
                            lastInsertion = addNode(resultado, lastInsertion, actM1);
                            actM1 = actM1.getNext();
                        }else {
                            lastInsertion = addNode(resultado, lastInsertion, actM2);
                            actM2 = actM2.getNext();
                        }
                    }else if (actM1.getFila() < actM2.getFila()){
                        lastInsertion = addNode(resultado,lastInsertion,actM1);
                        actM1 = actM1.getNext();
                    }else{
                        lastInsertion = addNode(resultado,lastInsertion,actM2);
                        actM2 = actM2.getNext();
                    }
                }else{
                    lastInsertion = addNode(resultado,lastInsertion,actM1);
                    actM1 = null;
                }
            }else{
                lastInsertion = addNode( resultado,lastInsertion,actM2);
                actM2 = null;
            }
        }

        return resultado;
    }

    private Node addNode(Matriz resultado, Node lastInsertion, Node actM1) {
        if (lastInsertion != null){
            lastInsertion.setNext(actM1);
        }else{
            resultado.setFirst(actM1);
        }
        lastInsertion = actM1;
        return lastInsertion;
    }

    public void setFirst(Node first) {
        this.first = first;
    }

    public void display() {
        if (this.first != null){
            this.first.display();
        }else{
            System.out.println("vacio");
        }
    }
}
