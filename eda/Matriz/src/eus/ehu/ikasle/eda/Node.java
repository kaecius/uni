package eus.ehu.ikasle.eda;

public class Node {

    private int dato;
    private int fila;
    private int columna;
    private Node next;

    public Node(int dato, int fila, int columna) {
        this.dato = dato;
        this.fila = fila;
        this.columna = columna;
    }

    public int getDato() {
        return dato;
    }

    public int getFila() {
        return fila;
    }

    public int getColumna() {
        return columna;
    }

    public Node getNext() {
        return next;
    }

    public Node setNext(Node node){
        this.next = node;
        return this.next;
    }

    public void display(){
        Node actual = this;
        do{
            System.out.printf("[%d,%d] = %d --> ",actual.fila,actual.columna,actual.dato);
            actual = actual.next;
        }while(actual != null);
        System.out.print("||");
        System.out.println();
    }

    @Override
    public String toString() {
        return "Node{" +
                "dato=" + dato +
                ", fila=" + fila +
                ", columna=" + columna +
                '}';
    }
}
