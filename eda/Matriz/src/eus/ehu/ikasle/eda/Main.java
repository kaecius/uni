package eus.ehu.ikasle.eda;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
        Matriz m1 = new Matriz();
        Matriz m2 = new Matriz();
        Matriz resultado;


        resultado = m1.suma(m1,m2);
        resultado.display();

        Node firstM1 = new Node(3,2,2);
        firstM1.setNext(new Node(5,2,4))
                .setNext(new Node(1,3,1))
                .setNext(new Node(2,4,4));

        Node firstM2 = new Node(9,1,1);
        firstM2.setNext(new Node(7,2,2))
                .setNext(new Node(2,3,3))
                .setNext(new Node(3,4,4));

        m1.setFirst(firstM1);


        resultado = m1.suma(m1,m2);
        resultado.display();

        m1.setFirst(null);
        m2.setFirst(firstM2);


        resultado = m1.suma(m1,m2);
        resultado.display();

        m1.setFirst(firstM1);


        Date date = new Date();
        resultado = m1.suma(m1,m2);
        //System.out.println(new Date().getTime() - date.getTime());
        resultado.display();

    }

}
