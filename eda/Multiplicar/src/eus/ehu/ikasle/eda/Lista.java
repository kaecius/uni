package eus.ehu.ikasle.eda;

public class Lista<T> {

    private Node<T> first;

    public Lista(Node<T> first) {
        this.first = first;
    }


    public void multiplicar(Integer n){
        Node<T> actual = this.first;
        Node<T> previous,copiaActual;
        if (this.first != null){
            do{
                previous = actual.getPrev();
                copiaActual = actual;
                for (int i = 1; i < n ; i++){
                    Node<T> copia = new Node<>(actual.getData());
                    copia.setPrev(previous);
                    copia.setNext(copiaActual);
                    previous.setNext(copia);
                    copiaActual.setPrev(copia);
                    copiaActual = copia;
                }
                actual = actual.getNext();
            }while(!actual.getData().equalsIgnoreCase(this.first.getData()));
            if (this.first != actual) {
                this.first = actual;
            }
        }
    }

    public void print(){
        Node<T> actual = this.first;
        do {
            System.out.println(actual.getPrev().getData() + "<--" + actual.getData() + " --> " + actual.getNext().getData());
            actual = actual.getNext();
        }while(actual != this.first);
    }

}
