package eus.ehu.ikasle.eda;

public class Main {

    public static void main(String[] args) {
        Node<Integer> first = new Node<Integer>("1");
        first.setPrev(first);
        first.setNext(first);

        Lista<Integer> lista = new Lista<Integer>(first);
        lista.multiplicar(3);
        lista.print();
        first = new Node<Integer>("1");
        Node<Integer> second = new Node<>("2");
        Node<Integer> third = new Node<>("3");

        first.setPrev(third);
        first.setNext(second);
        second.setPrev(first);
        second.setNext(third);
        third.setPrev(second);
        third.setNext(first);

        lista = new Lista<Integer>(first);
        lista.multiplicar(1);
        lista.print();
        lista.multiplicar(3);
        lista.print();

    }


}
