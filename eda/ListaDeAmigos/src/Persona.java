public class Persona {

    private String id;
    private Persona[] amigos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Persona[] getAmigos() {
        return amigos;
    }

    public void setAmigos(Persona[] amigos) {
        this.amigos = amigos;
    }

    public void eliminarApariciones() {
        for (Persona amigo :
                amigos) {
            int i;
            for (i = 0 ; i < amigo.amigos.length && !amigo.amigos[i].id.equals(this.id);i++);
            if (i < amigo.amigos.length){
                amigo.amigos[i] = null;
            }
        }
    }
}
