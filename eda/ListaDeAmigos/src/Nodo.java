public class Nodo {

    private Persona info;
    private Nodo next;

    public Nodo(Persona info) {
        this.info = info;
    }

    public Persona getInfo() {
        return info;
    }

    public void setInfo(Persona info) {
        this.info = info;
    }

    public Nodo getNext() {
        return next;
    }

    public void setNext(Nodo next) {
        this.next = next;
    }
}
