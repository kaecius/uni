public class Lista {

    private Nodo primero;


    public void eliminar(String id){
        Nodo actual = primero;
        Nodo prev = null;
        while(actual != null && !actual.getInfo().getId().equals(id)){
            prev = actual;
            actual = actual.getNext();
        }
        if (actual != null && actual.getInfo().getId().equals(id)){
            actual.getInfo().eliminarApariciones();
            if (prev == null){
                this.primero = actual.getNext();
            }else{
                prev.setNext(actual.getNext());
            }
        }
    }

}
