with datos,Ada.Integer_Text_IO,Ada.Text_IO;
use datos,Ada.Integer_Text_IO,Ada.Text_IO;

with pertenece_a_preferencias_del_hombre, ordenar_parejas,escribir_lp, escribir_parejas;----....añadir los subprogramas que necesitéis

procedure crear_parejas_estables (LP: in out Lista_parejas; posible: out Boolean) is
--Pre:	LP contiene la lista de parejas sin ordenar por edad y con parejas que no son necesariamente estables;
--Pos: 	si ha sido posible generar parejas estables, posible será true y LP contendrá la lista de parejas ordenadas
--      por edad y con parejas que son estables porque respetan las preferencias de todo el mundo; si no, posible
--      valdrá false y LP se quedará como al comienzo;
   copia_LP : Lista_parejas := LP;
   i,j,k: integer;
   asignado : boolean := false;
   tmp_hombre : Parte_pareja; 
begin
   ordenar_parejas(LP);
   i:=1;
   j:=1;
   k:=1;
   posible:=True;
   while posible and i <= LP.Cuantas-1 loop
      Put_Line(Integer'Image(i));
      while j <= LP.Parejas(i)(1).Preferencias.Cuantas_pers and not asignado loop
         -- no se encuentre entre las preferencias del hombre
         while k <= LP.Cuantas and LP.Parejas(k)(2).Nombre /= LP.Parejas(i)(1).Preferencias.Personas(j) loop
            k:= k+1;
         end loop;
         if pertenece_a_preferencias_del_hombre(LP.Parejas(k)(2).Preferencias,LP.Parejas(i)(1).Nombre) then
            if LP.Parejas(k)(1).Edad < LP.Parejas(i)(1).Edad then
               tmp_hombre := LP.Parejas(i)(2);
               LP.Parejas(i)(2) := LP.Parejas(k)(2);
               LP.Parejas(k)(2) := tmp_hombre;
               asignado := true;
            elsif LP.Parejas(k)(1) = LP.Parejas(i)(1) then
               asignado := true;
            else
               j:=j+1;
            end if;
         else 
            j := j+1;
         end if;
         k:= 1;
      end loop;
     -- escribir_parejas(Lp);
      --Put_Line("Asignado : "& Boolean'Image(asignado));
      if not asignado then
         posible := false;
         LP := copia_LP;
      else
         i:= i+1;
         asignado := false;
      end if;
      --Put_Line("Posible : "& Boolean'Image(posible));
   end loop;
   if i= LP.Cuantas and not posible then
      posible := false;
      LP := copia_LP;
   end if;
 end crear_parejas_estables;
