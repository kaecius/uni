with datos;
use datos;
with Ada.Text_IO; use Ada.Text_IO;

function pertenece_a_preferencias_del_hombre( pref_h1: Lista_preferencias; nombre_mujer: Nombre_pila) return boolean  is
   pertenece : Boolean := false;
   i : integer := 1;
begin
   while i<= pref_h1.Cuantas_pers and not pertenece loop
      --  Put_Line(pref_h1.Personas(i) & " = " & nombre_mujer);
      if pref_h1.Personas(i) = nombre_mujer then
         pertenece := true;
      else 
         i := i +1;
      end if;
   end loop;
   --Put_Line("Pertenece :" & Boolean'Image(pertenece));
   return pertenece;
end pertenece_a_preferencias_del_hombre;
