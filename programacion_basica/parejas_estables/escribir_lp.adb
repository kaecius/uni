with datos,Ada.Integer_Text_IO,Ada.Text_IO;
use datos,Ada.Integer_Text_IO,Ada.Text_IO;
   
procedure escribir_LP (LP: in Lista_parejas) is
   
begin
   for i in 1..LP.Cuantas loop
      put(LP.Parejas(i)(1).Nombre(1..10));      
      put(",");
      put(Lp.Parejas(i)(1).Edad, Width=>0);
      put("   Y su pareja actual es: ");
      put(Lp.Parejas(i)(2).Nombre(1..10));
      new_line;   
      put("   Preferencias de : ");
      put(LP.Parejas(i)(1).Nombre(1..10));  
      new_line;
      put("         ");  
      for j in 1..LP.Parejas(i)(1).Preferencias.Cuantas_pers loop
         put(LP.Parejas(i)(1).Preferencias.Personas(j)(1..10));
      end loop;
      new_line;
      
      new_line;
      put("    Preferencias de: ");
      put(LP.Parejas(i)(2).Nombre);
      new_line;
      put("         ");    
      for j in 1..LP.Parejas(i)(2).Preferencias.Cuantas_pers loop
         put(LP.Parejas(i)(2).Preferencias.Personas(j)(1..10));
      end loop;
      new_line(3);
   end loop;
   new_line;      
end escribir_LP;