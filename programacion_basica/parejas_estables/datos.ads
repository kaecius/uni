package datos is
  
  N: constant integer :=10;
  subtype Nombre_pila is string(1..20);
  type T_personas is array (1..N) of Nombre_pila;
  type Lista_preferencias is record
	Cuantas_pers: integer;
	Personas: T_personas;
  end record;

  type Parte_pareja is record
	Hombre:Boolean; ---que concordar� con el �ndice del array de tipo 
               ---Una_pareja, de forma que la mujer ir� en la 
               ---posici�n 1 y el  hombre en la posici�n 2
    Nombre: Nombre_pila;
    Edad: integer;
	Preferencias: Lista_preferencias;
  end record;

  type Una_pareja is array (1..2) of Parte_pareja;
  type T_parejas is array(1.. N) of Una_pareja;
  type Lista_parejas is record
	Cuantas: integer;
	Parejas: T_parejas;
  end record;
  
end datos;