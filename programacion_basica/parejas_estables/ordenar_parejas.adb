with datos;
use datos;

procedure ordenar_parejas (Todas_parejas: in out Lista_parejas) is
--Pre:	�Todas_parejas� contiene la lista con todas las parejas de baile pero sin ordenar 
--Pos: 	�Todas_parejas� contiene la lista con todas las parejas de baile ordenadas de mayor
--      a menor edad de las damas

-- implementaci�n del m�todo de la burbuja
   aux:Una_pareja;
   begin
      for i in 1..Todas_parejas.Cuantas-1 loop
         for j in 1..Todas_parejas.Cuantas-1 loop
            if Todas_parejas.Parejas(j)(1).Edad < Todas_parejas.Parejas(j+1)(1).Edad then
               aux:=Todas_parejas.Parejas(j);
               Todas_parejas.Parejas(j):=Todas_parejas.Parejas(j+1);
               Todas_parejas.Parejas(j+1):=aux;
            end if;   
         end loop;                     
      end loop;
end ordenar_parejas;