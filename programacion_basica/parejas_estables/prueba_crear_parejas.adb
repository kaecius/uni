with datos,Ada.Text_IO;
use datos,Ada.Text_IO;
with crear_parejas_estables,escribir_lp,escribir_parejas;

procedure Prueba_Crear_Parejas is
   
   Posible:boolean;
   Lp: Lista_Parejas;
   
   begin
   ---prueba_1
   ---rellenar la estructura con datos tal que sea posible hacer las parejas estables   
   put_line("Los datos para la primera prueba son: ");
   Lp.Cuantas:=5;
   ---inf primera pareja
   Lp.Parejas(1)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(1)(1).Nombre:="Miren               ";
   Lp.Parejas(1)(1).Edad:=23;
   Lp.Parejas(1)(1).Preferencias.Cuantas_pers:=3;
                                            ----12345678901234567890
   Lp.Parejas(1)(1).Preferencias.Personas(1):="Benat               ";
   Lp.Parejas(1)(1).Preferencias.Personas(2):="Josu                ";
   Lp.Parejas(1)(1).Preferencias.Personas(3):="Imanol              ";

   Lp.Parejas(1)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(1)(2).Nombre:="Aitor               ";
   Lp.Parejas(1)(2).Edad:=21;
   Lp.Parejas(1)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(1)(2).Preferencias.Personas(1):="Saioa               ";
   Lp.Parejas(1)(2).Preferencias.Personas(2):="Amaia               ";
   ---inf segunda pareja
   Lp.Parejas(2)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(2)(1).Nombre:="Saioa               ";
   Lp.Parejas(2)(1).Edad:=20;
   Lp.Parejas(2)(1).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(2)(1).Preferencias.Personas(1):="Benat               ";
   Lp.Parejas(2)(1).Preferencias.Personas(2):="Aitor               ";

   Lp.Parejas(2)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(2)(2).Nombre:="Josu                ";
   Lp.Parejas(2)(2).Edad:=21;
   Lp.Parejas(2)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(2)(2).Preferencias.Personas(1):="Amaia               ";
   Lp.Parejas(2)(2).Preferencias.Personas(2):="Miren               ";
  
  ---inf tercera pareja
   Lp.Parejas(3)(1).Hombre:=False;
                          ---12345678901234567890
   Lp.Parejas(3)(1).Nombre:="Amaia               ";
   Lp.Parejas(3)(1).Edad:=21;
   Lp.Parejas(3)(1).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(3)(1).Preferencias.Personas(1):="Josu                ";
   Lp.Parejas(3)(1).Preferencias.Personas(2):="Aitor               ";

   Lp.Parejas(3)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(3)(2).Nombre:="Benat               ";
   Lp.Parejas(3)(2).Edad:=21;
   Lp.Parejas(3)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(3)(2).Preferencias.Personas(1):="Itziar              ";
   Lp.Parejas(3)(2).Preferencias.Personas(2):="Miren               ";


   ---inf cuarta pareja
   Lp.Parejas(4)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(4)(1).Nombre:="Ainara              ";
   Lp.Parejas(4)(1).Edad:=27;
   Lp.Parejas(4)(1).Preferencias.Cuantas_pers:=3;
                                            ----12345678901234567890
   Lp.Parejas(4)(1).Preferencias.Personas(1):="Imanol              ";
   Lp.Parejas(4)(1).Preferencias.Personas(2):="Aitor               ";
   Lp.Parejas(4)(1).Preferencias.Personas(3):="Josu                ";


   Lp.Parejas(4)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(4)(2).Nombre:="Asier               ";
   Lp.Parejas(4)(2).Edad:=21;
   Lp.Parejas(4)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(4)(2).Preferencias.Personas(1):="Itziar              ";
   Lp.Parejas(4)(2).Preferencias.Personas(2):="Miren               ";

 ---inf quinta pareja
   Lp.Parejas(5)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(5)(1).Nombre:="Itziar              ";
   Lp.Parejas(5)(1).Edad:=26;
   Lp.Parejas(5)(1).Preferencias.Cuantas_pers:=3;
                                            ----12345678901234567890
   Lp.Parejas(5)(1).Preferencias.Personas(1):="Asier               ";
   Lp.Parejas(5)(1).Preferencias.Personas(2):="Aitor               ";
   Lp.Parejas(5)(1).Preferencias.Personas(3):="Josu                ";


   Lp.Parejas(5)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(5)(2).Nombre:="Imanol              ";
   Lp.Parejas(5)(2).Edad:=21;
   Lp.Parejas(5)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(5)(2).Preferencias.Personas(1):="Ainara              ";
   Lp.Parejas(5)(2).Preferencias.Personas(2):="Miren               ";

   escribir_lp(Lp);
   put_line("Tu programa deber�a escribir");
   put_line("Ainara-Imanol");
   put_line("Itziar-Asier");
   put_line("Miren-Benat");
   put_line("Amaia-Josu");
   put_line("Saioa-Aitor");
   new_line;
   put_line("pulsa return para saber las parejas que obtiene tu programa");
   skip_line;
   
   crear_parejas_estables(Lp,Posible);
   if(Posible) then 
        escribir_parejas(Lp);
   else
      put_line ("No se pueden mejorar las parejas");
      
   end if;
   Skip_Line;   
   ---prueba_2
   ---rellenar la estructura con datos tal que sea posible hacer las parejas estables   
   Lp.Cuantas:=5;
   ---inf primera pareja
   Lp.Parejas(1)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(1)(1).Nombre:="Miren               ";
   Lp.Parejas(1)(1).Edad:=23;
   Lp.Parejas(1)(1).Preferencias.Cuantas_pers:=3;
                                            ----12345678901234567890
   Lp.Parejas(1)(1).Preferencias.Personas(1):="Benat               ";
   Lp.Parejas(1)(1).Preferencias.Personas(2):="Josu                ";
   Lp.Parejas(1)(1).Preferencias.Personas(3):="Imanol              ";

   Lp.Parejas(1)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(1)(2).Nombre:="Aitor               ";
   Lp.Parejas(1)(2).Edad:=21;
   Lp.Parejas(1)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(1)(2).Preferencias.Personas(1):="Saioa               ";
   Lp.Parejas(1)(2).Preferencias.Personas(2):="Amaia               ";

     ---inf segunda pareja
   Lp.Parejas(2)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(2)(1).Nombre:="Saioa               ";
   Lp.Parejas(2)(1).Edad:=20;
   Lp.Parejas(2)(1).Preferencias.Cuantas_pers:=2;
                                           ----12345678901234567890
   Lp.Parejas(2)(1).Preferencias.Personas(1):="Aitor               ";   
   Lp.Parejas(2)(1).Preferencias.Personas(2):="Benat               ";
  

   Lp.Parejas(2)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(2)(2).Nombre:="Josu                ";
   Lp.Parejas(2)(2).Edad:=21;
   Lp.Parejas(2)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(2)(2).Preferencias.Personas(1):="Amaia               ";
   Lp.Parejas(2)(2).Preferencias.Personas(2):="Miren               ";
  
  ---inf tercera pareja
   Lp.Parejas(3)(1).Hombre:=False;
                          ---12345678901234567890
   Lp.Parejas(3)(1).Nombre:="Amaia               ";
   Lp.Parejas(3)(1).Edad:=21;
   Lp.Parejas(3)(1).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(3)(1).Preferencias.Personas(1):="Josu                ";
   Lp.Parejas(3)(1).Preferencias.Personas(2):="Aitor               ";

   Lp.Parejas(3)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(3)(2).Nombre:="Benat               ";
   Lp.Parejas(3)(2).Edad:=21;
   Lp.Parejas(3)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(3)(2).Preferencias.Personas(1):="Itziar              ";
   Lp.Parejas(3)(2).Preferencias.Personas(2):="Saioa               ";


   ---inf cuarta pareja
   Lp.Parejas(4)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(4)(1).Nombre:="Ainara              ";
   Lp.Parejas(4)(1).Edad:=27;
   Lp.Parejas(4)(1).Preferencias.Cuantas_pers:=3;
                                            ----12345678901234567890
   Lp.Parejas(4)(1).Preferencias.Personas(1):="Imanol              ";
   Lp.Parejas(4)(1).Preferencias.Personas(2):="Aitor               ";
   Lp.Parejas(4)(1).Preferencias.Personas(3):="Josu                ";


   Lp.Parejas(4)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(4)(2).Nombre:="Asier               ";
   Lp.Parejas(4)(2).Edad:=21;
   Lp.Parejas(4)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(4)(2).Preferencias.Personas(1):="Itziar              ";
   Lp.Parejas(4)(2).Preferencias.Personas(2):="Miren               ";

 ---inf quinta pareja
   Lp.Parejas(5)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(5)(1).Nombre:="Itziar              ";
   Lp.Parejas(5)(1).Edad:=26;
   Lp.Parejas(5)(1).Preferencias.Cuantas_pers:=3;
                                            ----12345678901234567890
   Lp.Parejas(5)(1).Preferencias.Personas(1):="Asier               ";
   Lp.Parejas(5)(1).Preferencias.Personas(2):="Aitor               ";
   Lp.Parejas(5)(1).Preferencias.Personas(3):="Josu                ";


   Lp.Parejas(5)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(5)(2).Nombre:="Imanol              ";
   Lp.Parejas(5)(2).Edad:=21;
   Lp.Parejas(5)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(5)(2).Preferencias.Personas(1):="Ainara              ";
   Lp.Parejas(5)(2).Preferencias.Personas(2):="Miren               ";

   escribir_lp(Lp);
    put_line("Tu programa deber�a escribir");
    put_line("Ainara-Imanol");
    put_line("Itziar-Asier");
    put_line("Miren-Josu");
    put_line("Amaia-Aitor");
    put_line("Saioa-Benat");
    new_line;
    put_line("pulsa return para saber las parejas que obtiene tu programa");
    skip_line;
   
    crear_parejas_estables(Lp,Posible);
    if(Posible) then 
        escribir_parejas(Lp);
   else
       put_line ("No se pueden mejorar las parejas");
   end if;
Skip_Line; 

 ---prueba_3
   ---rellenar la estructura con datos tal que sea posible hacer las parejas estables   
   Lp.Cuantas:=5;
   ---inf primera pareja
   Lp.Parejas(1)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(1)(1).Nombre:="Miren               ";
   Lp.Parejas(1)(1).Edad:=23;
   Lp.Parejas(1)(1).Preferencias.Cuantas_pers:=3;
                                            ----12345678901234567890
   Lp.Parejas(1)(1).Preferencias.Personas(1):="Benat               ";
   Lp.Parejas(1)(1).Preferencias.Personas(2):="Josu                ";
   Lp.Parejas(1)(1).Preferencias.Personas(3):="Imanol              ";

   Lp.Parejas(1)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(1)(2).Nombre:="Aitor               ";
   Lp.Parejas(1)(2).Edad:=21;
   Lp.Parejas(1)(2).Preferencias.Cuantas_pers:=1;
                                            ----12345678901234567890
   Lp.Parejas(1)(2).Preferencias.Personas(1):="Saioa               ";
     ---inf segunda pareja
   Lp.Parejas(2)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(2)(1).Nombre:="Saioa               ";
   Lp.Parejas(2)(1).Edad:=20;
   Lp.Parejas(2)(1).Preferencias.Cuantas_pers:=2;
                                           ----12345678901234567890
   Lp.Parejas(2)(1).Preferencias.Personas(1):="Aitor               ";   
   Lp.Parejas(2)(1).Preferencias.Personas(2):="Benat               ";
  

   Lp.Parejas(2)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(2)(2).Nombre:="Josu                ";
   Lp.Parejas(2)(2).Edad:=21;
   Lp.Parejas(2)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(2)(2).Preferencias.Personas(1):="Amaia               ";
   Lp.Parejas(2)(2).Preferencias.Personas(2):="Miren               ";
  
  ---inf tercera pareja
   Lp.Parejas(3)(1).Hombre:=False;
                          ---12345678901234567890
   Lp.Parejas(3)(1).Nombre:="Amaia               ";
   Lp.Parejas(3)(1).Edad:=21;
   Lp.Parejas(3)(1).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(3)(1).Preferencias.Personas(1):="Josu                ";
   Lp.Parejas(3)(1).Preferencias.Personas(2):="Aitor               ";

   Lp.Parejas(3)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(3)(2).Nombre:="Benat               ";
   Lp.Parejas(3)(2).Edad:=21;
   Lp.Parejas(3)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(3)(2).Preferencias.Personas(1):="Itziar              ";
   Lp.Parejas(3)(2).Preferencias.Personas(2):="Ainara              ";


   ---inf cuarta pareja
   Lp.Parejas(4)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(4)(1).Nombre:="Ainara              ";
   Lp.Parejas(4)(1).Edad:=27;
   Lp.Parejas(4)(1).Preferencias.Cuantas_pers:=3;
                                            ----12345678901234567890
   Lp.Parejas(4)(1).Preferencias.Personas(1):="Imanol              ";
   Lp.Parejas(4)(1).Preferencias.Personas(2):="Aitor               ";
   Lp.Parejas(4)(1).Preferencias.Personas(3):="Josu                ";


   Lp.Parejas(4)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(4)(2).Nombre:="Asier               ";
   Lp.Parejas(4)(2).Edad:=21;
   Lp.Parejas(4)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(4)(2).Preferencias.Personas(1):="Itziar              ";
   Lp.Parejas(4)(2).Preferencias.Personas(2):="Miren               ";

 ---inf quinta pareja
   Lp.Parejas(5)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(5)(1).Nombre:="Itziar              ";
   Lp.Parejas(5)(1).Edad:=26;
   Lp.Parejas(5)(1).Preferencias.Cuantas_pers:=3;
                                            ----12345678901234567890
   Lp.Parejas(5)(1).Preferencias.Personas(1):="Asier               ";
   Lp.Parejas(5)(1).Preferencias.Personas(2):="Aitor               ";
   Lp.Parejas(5)(1).Preferencias.Personas(3):="Josu                ";


   Lp.Parejas(5)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(5)(2).Nombre:="Imanol              ";
   Lp.Parejas(5)(2).Edad:=21;
   Lp.Parejas(5)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(5)(2).Preferencias.Personas(1):="Ainara              ";
   Lp.Parejas(5)(2).Preferencias.Personas(2):="Miren               ";

   escribir_lp(LP);
   put_line("Tu programa deber�a dejar la estructura como estaba ya que no es posible mejorarla");
   put_line("porque Amaia prefiere a Josu (que ya est� emparejado) y luego a Aitor que no la prefiere a ella, ");
   put_line("y ya le hab�a sido previamente asignado a Ainara que es la mujer de m�s edad"); 
   new_line;
   put_line("pulsa return para saber las parejas que obtiene tu programa");
   skip_line;
   crear_parejas_estables(Lp,Posible);
   if(Posible) then 
        escribir_parejas(Lp);
   else
       put_line ("No se pueden mejorar las parejas");
   end if;
Skip_Line; 
   
   Lp.Cuantas:=5;
   ---inf primera pareja
   Lp.Parejas(1)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(1)(1).Nombre:="Miren               ";
   Lp.Parejas(1)(1).Edad:=23;
   Lp.Parejas(1)(1).Preferencias.Cuantas_pers:=1;
                                            ----12345678901234567890
     Lp.Parejas(1)(1).Preferencias.Personas(1):="Imanol              ";

   Lp.Parejas(1)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(1)(2).Nombre:="Aitor               ";
   Lp.Parejas(1)(2).Edad:=21;
   Lp.Parejas(1)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(1)(2).Preferencias.Personas(1):="Saioa               ";
   Lp.Parejas(1)(2).Preferencias.Personas(2):="Amaia               ";

     ---inf segunda pareja
   Lp.Parejas(2)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(2)(1).Nombre:="Saioa               ";
   Lp.Parejas(2)(1).Edad:=20;
   Lp.Parejas(2)(1).Preferencias.Cuantas_pers:=2;
                                           ----12345678901234567890
   Lp.Parejas(2)(1).Preferencias.Personas(1):="Aitor               ";   
   Lp.Parejas(2)(1).Preferencias.Personas(2):="Benat               ";
  

   Lp.Parejas(2)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(2)(2).Nombre:="Josu                ";
   Lp.Parejas(2)(2).Edad:=21;
   Lp.Parejas(2)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(2)(2).Preferencias.Personas(1):="Amaia               ";
   Lp.Parejas(2)(2).Preferencias.Personas(2):="Miren               ";
  
  ---inf tercera pareja
   Lp.Parejas(3)(1).Hombre:=False;
                          ---12345678901234567890
   Lp.Parejas(3)(1).Nombre:="Amaia               ";
   Lp.Parejas(3)(1).Edad:=21;
   Lp.Parejas(3)(1).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(3)(1).Preferencias.Personas(1):="Josu                ";
   Lp.Parejas(3)(1).Preferencias.Personas(2):="Aitor               ";

   Lp.Parejas(3)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(3)(2).Nombre:="Benat               ";
   Lp.Parejas(3)(2).Edad:=21;
   Lp.Parejas(3)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(3)(2).Preferencias.Personas(1):="Itziar              ";
   Lp.Parejas(3)(2).Preferencias.Personas(2):="Saioa               ";


   ---inf cuarta pareja
   Lp.Parejas(4)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(4)(1).Nombre:="Ainara              ";
   Lp.Parejas(4)(1).Edad:=27;
   Lp.Parejas(4)(1).Preferencias.Cuantas_pers:=3;
                                            ----12345678901234567890
   Lp.Parejas(4)(1).Preferencias.Personas(1):="Imanol              ";
   Lp.Parejas(4)(1).Preferencias.Personas(2):="Aitor               ";
   Lp.Parejas(4)(1).Preferencias.Personas(3):="Josu                ";


   Lp.Parejas(4)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(4)(2).Nombre:="Asier               ";
   Lp.Parejas(4)(2).Edad:=21;
   Lp.Parejas(4)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(4)(2).Preferencias.Personas(1):="Itziar              ";
   Lp.Parejas(4)(2).Preferencias.Personas(2):="Miren               ";

 ---inf quinta pareja
   Lp.Parejas(5)(1).Hombre:=False;
                           ---12345678901234567890
   Lp.Parejas(5)(1).Nombre:="Itziar              ";
   Lp.Parejas(5)(1).Edad:=26;
   Lp.Parejas(5)(1).Preferencias.Cuantas_pers:=3;
                                            ----12345678901234567890
   Lp.Parejas(5)(1).Preferencias.Personas(1):="Asier               ";
   Lp.Parejas(5)(1).Preferencias.Personas(2):="Aitor               ";
   Lp.Parejas(5)(1).Preferencias.Personas(3):="Josu                ";


   Lp.Parejas(5)(2).Hombre:=True;
                           ---12345678901234567890
   Lp.Parejas(5)(2).Nombre:="Imanol              ";
   Lp.Parejas(5)(2).Edad:=21;
   Lp.Parejas(5)(2).Preferencias.Cuantas_pers:=2;
                                            ----12345678901234567890
   Lp.Parejas(5)(2).Preferencias.Personas(1):="Ainara              ";
   Lp.Parejas(5)(2).Preferencias.Personas(2):="Miren               ";

   escribir_lp(LP);
  
   put_line("Tu programa deber�a dejar la estructura como estaba ya que no es posible mejorarla");
   put_line("porque la preferencia de Miren es solo Imanol, ");
   put_line("y ya le hab�a sido previamente asignado a Ainara que es la mujer de m�s edad"); 
   new_line;
   put_line("pulsa return para saber las parejas que obtiene tu programa");
   skip_line;
crear_parejas_estables(Lp,Posible);
   if(Posible) then 
       escribir_parejas(Lp);
   else
       put_line ("No se pueden mejorar las parejas");
   end if;
Skip_Line; 
end prueba_crear_parejas;
