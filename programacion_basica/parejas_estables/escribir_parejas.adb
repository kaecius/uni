with datos,Ada.Integer_Text_IO,Ada.Text_IO;
use datos,Ada.Integer_Text_IO,Ada.Text_IO;
   
procedure escribir_parejas (LP: in Lista_parejas) is
   
begin
   for i in 1..LP.Cuantas loop
      put(LP.Parejas(i)(1).Nombre(1..10));      
      put(",");
      put(LP.Parejas(i)(1).Edad, width=>0);
      put(" Y la nueva pareja es: ");
      put(LP.Parejas(i)(2).Nombre);
      new_line;
    end loop;
   new_line(4);      
end escribir_parejas;