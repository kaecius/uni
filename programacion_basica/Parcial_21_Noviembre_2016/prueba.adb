with datos,Lista_Asignaturas_ordenada; use datos;
with Ada.Text_IO; use Ada.Text_IO;
procedure prueba is
   lst_alumnos : Lista_Alumnos;
   lst_asig_alumn : Lista_Asignaturas_Alumnos;
begin
   lst_alumnos.Cuantos := 5;
   lst_alumnos.TA(1).DNI := 79079266;
   lst_alumnos.TA(1).Nombre_y_Apellidos := "Daniel Ca�adillas Patrici";
   lst_alumnos.TA(1).Nota_Media := 7.89;
   lst_alumnos.TA(1).LA.Cuantas := 2;
   lst_alumnos.TA(1).LA.T_Asig(1) := "Algebra                  ";
   lst_alumnos.TA(1).LA.T_Asig(2) := "Analisis                 ";

   lst_alumnos.TA(2).DNI := 79079286;
   lst_alumnos.TA(2).Nombre_y_Apellidos := "Ander Cejudo             ";
   lst_alumnos.TA(2).Nota_Media := 7.89;
   lst_alumnos.TA(2).LA.Cuantas := 3;
   lst_alumnos.TA(2).LA.T_Asig(1) := "Algebra                  ";
   lst_alumnos.TA(2).LA.T_Asig(2) := "Analisis                 ";
   lst_alumnos.TA(2).LA.T_Asig(3) := "FTC                      ";

   lst_alumnos.TA(3).DNI := 79579266;
   lst_alumnos.TA(3).Nombre_y_Apellidos := "Daniel Ca�adas           ";
   lst_alumnos.TA(3).Nota_Media := 7.89;
   lst_alumnos.TA(3).LA.Cuantas := 1;
   lst_alumnos.TA(3).LA.T_Asig(1) := "Algebra                  ";

   lst_alumnos.TA(4).DNI := 79079260;
   lst_alumnos.TA(4).Nombre_y_Apellidos := "Daniel  Patricio         ";
   lst_alumnos.TA(4).Nota_Media := 7.89;
   lst_alumnos.TA(4).LA.Cuantas := 4;
   lst_alumnos.TA(4).LA.T_Asig(1) := "Algebra                  ";
   lst_alumnos.TA(4).LA.T_Asig(2) := "Analisis                 ";
   lst_alumnos.TA(4).LA.T_Asig(3) := "FTC                      ";
   lst_alumnos.TA(4).LA.T_Asig(4) := "Calculo                  ";


   lst_alumnos.TA(5).DNI := 79056266;
   lst_alumnos.TA(5).Nombre_y_Apellidos := "Daniel                   ";
   lst_alumnos.TA(5).Nota_Media := 7.89;
   lst_alumnos.TA(5).LA.Cuantas := 2;
   lst_alumnos.TA(5).LA.T_Asig(1) := "FTC                      ";
   lst_alumnos.TA(5).LA.T_Asig(2) := "Calculo                  ";



   lst_asig_alumn :=   lista_asignaturas_ordenada(lst_alumnos);
   for i in 1..lst_asig_alumn.Cuantos loop
      put_line(lst_asig_alumn.L_Asignatura_Alumnos(i).Asignatura);
      for j in  1..lst_asig_alumn.L_Asignatura_Alumnos(i).Alumnos.Cuantos loop
         Put_Line("    " & lst_asig_alumn.L_Asignatura_Alumnos(i).Alumnos.TA(j).Nombre_y_Apellidos);
      end loop;
   end loop;
end prueba;
