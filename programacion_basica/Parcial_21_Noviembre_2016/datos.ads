package datos is
   NA : constant Integer := 50;
   subtype Nombre_Asignatura is String(1..25);
   type Tabla_Asignaturas is array (1..NA) of Nombre_Asignatura;
   type Lista_Asignaturas is record
      Cuantas : Integer;
      T_Asig : Tabla_Asignaturas;
   end record;
   type Datos_Alumno is record
      DNI : Integer;
      Nombre_y_Apellidos: String(1..25);
      Nota_Media : float;
      LA: Lista_Asignaturas;
   end record;
   type Tabla_Alumnos is array (1..NA) of Datos_Alumno;
   type Lista_Alumnos is record
      Cuantos : Integer;
      TA: Tabla_Alumnos;
   end record;
   type Asignatura_Alumnos is record
      Asignatura : Nombre_Asignatura;
      Alumnos : Lista_Alumnos;
   end record;
   type Ar_Asignatura_Alumnos is array (1..NA) of Asignatura_Alumnos ;
   type Lista_Asignaturas_Alumnos is record
      Cuantos: Integer;
      L_Asignatura_Alumnos : Ar_Asignatura_Alumnos;
   end record;
end datos;
