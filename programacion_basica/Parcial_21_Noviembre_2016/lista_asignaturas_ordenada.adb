with datos;
use datos;
with Ada.Text_IO; use Ada.Text_IO;

function lista_asignaturas_ordenada(LAlumn : in Lista_Alumnos) return Lista_Asignaturas_Alumnos is
   Lst_Asig_Alumn : Lista_Asignaturas_Alumnos;
   tmp_datos_alumnos : Datos_Alumno;
   tmp_asignatura : Nombre_Asignatura;
   function estaAsignatura(strAsignatura : Nombre_Asignatura;Lst_Asig_Alumn : Lista_Asignaturas_Alumnos) return Boolean is
      result : Boolean := false;
      i : integer := 1;
   begin
      --Put_Line("Comparando : " & Lst_Asig_Alumn.L_Asignatura_Alumnos(i).Asignatura & "=" & strAsignatura & ".");
      while i <= Lst_Asig_Alumn.Cuantos and not result loop
         if Lst_Asig_Alumn.L_Asignatura_Alumnos(i).Asignatura = strAsignatura then
            --Put_Line("Encontrado : " & Lst_Asig_Alumn.L_Asignatura_Alumnos(i).Asignatura & "=" & strAsignatura & ".");
            result := true;
         end if;
         i := i+1;
      end loop;
      return result;
   end estaAsignatura;

   procedure insertarAlumno(alumno : Datos_Alumno; asignatura : Nombre_Asignatura; Lst_Asig_Alumn : in out Lista_Asignaturas_Alumnos) is
      i : integer := 1;
   begin
      while i <= Lst_Asig_Alumn.Cuantos AND THEN Lst_Asig_Alumn.L_Asignatura_Alumnos(i).Asignatura /= asignatura loop
         i := i+1;
      end loop;
      Lst_Asig_Alumn.L_Asignatura_Alumnos(i).Alumnos.Cuantos := Lst_Asig_Alumn.L_Asignatura_Alumnos(i).Alumnos.Cuantos+1;
      Lst_Asig_Alumn.L_Asignatura_Alumnos(i).Alumnos.TA(Lst_Asig_Alumn.L_Asignatura_Alumnos(i).Alumnos.Cuantos) := alumno;
   end insertarAlumno;

   procedure ordenar_lista(lst :in out Lista_Asignaturas_Alumnos) is
      tmp : Asignatura_Alumnos;
   begin
      for i in 1..lst.Cuantos loop
         for j in i..lst.Cuantos loop
            if lst.L_Asignatura_Alumnos(i).Alumnos.Cuantos < lst.L_Asignatura_Alumnos(j).Alumnos.Cuantos then
               tmp := lst.L_Asignatura_Alumnos(i);
               lst.L_Asignatura_Alumnos(i) := lst.L_Asignatura_Alumnos(j);
               lst.L_Asignatura_Alumnos(j) := tmp;
            end if;
         end loop;
      end loop;
   end ordenar_lista;

begin
   Lst_Asig_Alumn.Cuantos := 0;
   for i in 1..LAlumn.Cuantos loop
      tmp_datos_alumnos := LAlumn.TA(i);
      for j in 1..tmp_datos_alumnos.LA.Cuantas loop
         tmp_asignatura := tmp_datos_alumnos.LA.T_Asig(j);
         --Put_Line(tmp_asignatura);
         if not estaAsignatura(tmp_asignatura,Lst_Asig_Alumn) then
            --Put_Line(tmp_asignatura & "No Esta");
            Lst_Asig_Alumn.Cuantos := Lst_Asig_Alumn.Cuantos +1;
            --put(Integer'Image(Lst_Asig_Alumn.Cuantos));
            Lst_Asig_Alumn.L_Asignatura_Alumnos(Lst_Asig_Alumn.Cuantos).Asignatura := tmp_asignatura;
         end if;
         insertarAlumno(tmp_datos_alumnos,tmp_asignatura,Lst_Asig_Alumn);
      end loop;
   end loop;
   ordenar_lista(Lst_Asig_Alumn);

   return Lst_Asig_Alumn;
end lista_asignaturas_ordenada;
