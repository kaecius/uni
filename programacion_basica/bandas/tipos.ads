package tipos is
 Max_Bandas: constant integer:=500;
 Max_Albumes: constant Integer:=50;
 subtype S_Nombre is string(1..30);
 subtype Num_Bandas is Integer range 1..Max_Bandas;
 subtype Num_Albumes is Integer range 1..Max_Albumes;

 type R_Album is record
 Nombre_Album: S_Nombre;
 Anio: Positive;
 end record;
 type T_Albumes is array (1..Max_Albumes) of R_Album;
 type R_Albumes is record
 Cuantos_Albumes: Num_Albumes;
 Albumes:T_Albumes;
 end record;

 type R_Banda is record
 Nombre_Banda: S_Nombre;
 Info_Albumes: R_Albumes;
 end record;
 type T_Bandas is array (1..Max_Bandas) of R_Banda;

 type R_Discoteca is record
 Cuantas_Bandas: Num_Bandas;
 Bandas:T_Bandas;
 end record;
end tipos;
