with tipos;
with banda_mas_prolifica;
use tipos;
with Ada.Text_IO; use Ada.Text_IO;

procedure prueba_mas_prolifica IS
   Todas_Bandas:R_Discoteca;
   rdo:S_Nombre;
Begin
   --Caso1

   Todas_Bandas.Cuantas_Bandas:=3;
   Todas_Bandas.Bandas(1).Nombre_Banda:="RUNNING WILD                  ";
   Todas_Bandas.Bandas(1).Info_Albumes.Cuantos_Albumes:=11;
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(1).Nombre_Album:="Gates de Purgatory            ";
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(2).Nombre_Album:="Branded and Exiled            ";
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(3).Nombre_Album:="Under Jolly roger             ";
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(4).Nombre_Album:="Port royal                    ";
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(5).Nombre_Album:="Death or glory                ";
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(6).Nombre_Album:="Blazon Stone                  ";
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(7).Nombre_Album:="Pile of Skulls                ";
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(8).Nombre_Album:="Black Hand Inn                ";
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(9).Nombre_Album:="Masquerade                    ";
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(10).Nombre_Album:="The Rivalry                   ";
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(11).Nombre_Album:="Victory                       ";
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(1).Anio:=1984;
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(2).Anio:=1985;
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(3).Anio:=1987;
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(4).Anio:=1988;
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(5).Anio:=1989;
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(6).Anio:=1991;
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(7).Anio:=1992;
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(8).Anio:=1994;
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(9).Anio:=1995;
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(10).Anio:=1998;
   Todas_Bandas.Bandas(1).Info_Albumes.Albumes(11).Anio:=2000;

   Todas_Bandas.Bandas(2).Nombre_Banda:="IRON MAIDEN                   ";
   Todas_Bandas.Bandas(2).Info_Albumes.Cuantos_Albumes:=10;
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(1).Nombre_Album:="The Number Of The Beast       ";
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(2).Nombre_Album:="Piece Of Mind                 ";
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(3).Nombre_Album:="Power Slave                   ";
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(4).Nombre_Album:="Somewhere in Time             ";
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(5).Nombre_Album:="Seventh Son of a Seventh Son  ";
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(6).Nombre_Album:="No Prayer for the Dying       ";
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(7).Nombre_Album:="Fear of the Dark              ";
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(8).Nombre_Album:="The X Factor                  ";
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(9).Nombre_Album:="Virtual XI                    ";
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(10).Nombre_Album:="Brave New World               ";
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(1).Anio:=1982;
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(2).Anio:=1983;
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(3).Anio:=1984;
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(4).Anio:=1986;
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(5).Anio:=1988;
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(6).Anio:=1990;
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(7).Anio:=1992;
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(8).Anio:=1995;
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(9).Anio:=1998;
   Todas_Bandas.Bandas(2).Info_Albumes.Albumes(10).Anio:=2000;

   Todas_Bandas.Bandas(3).Nombre_Banda:="HELLOWEEN                     ";
   Todas_Bandas.Bandas(3).Info_Albumes.Cuantos_Albumes:=10;
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(1).Nombre_Album:="Helloween                     ";
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(2).Nombre_Album:="Walls of Jericho              ";
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(3).Nombre_Album:="Keeper Of The Seven Keys PartI";
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(4).Nombre_Album:="Keeper Of The Seven Keys Part2";
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(5).Nombre_Album:="Pink Bubbles go Ape           ";
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(6).Nombre_Album:="Chameleon                     ";
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(7).Nombre_Album:="Master Of The Rings           ";
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(8).Nombre_Album:="The Time Of The Oath          ";
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(9).Nombre_Album:="Better Than Raw               ";
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(10).Nombre_Album:="The Dark Ride                 ";
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(1).Anio:=1985;
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(2).Anio:=1985;
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(3).Anio:=1987;
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(4).Anio:=1988;
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(5).Anio:=1991;
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(6).Anio:=1993;
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(7).Anio:=1994;
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(8).Anio:=1996;
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(9).Anio:=1998;
   Todas_Bandas.Bandas(3).Info_Albumes.Albumes(10).Anio:=2000;
   rdo:=banda_mas_prolifica(Todas_Bandas,1975,1978);
   if rdo/="NINGUNA" then
      put(rdo);
   else
      put("La banda que mas albums ha publicado o una de ellas: "); put(rdo);
   end if;
END prueba_mas_prolifica;
