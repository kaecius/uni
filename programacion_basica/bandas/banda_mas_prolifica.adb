WITH Tipos, ADA.Text_IO;
USE Tipos, Ada.Text_IO;

FUNCTION Banda_Mas_Prolifica (
      Discoteca : R_Discoteca;
      Inicio    : Positive;
      Fin       : Positive)
  RETURN String IS
   Banda : S_NOMBRE := "NINGUNA                       ";
   Mayor,
   Cont  : Integer  := 0;
BEGIN
   FOR I IN 1..Discoteca.Cuantas_Bandas LOOP
      Cont := 0;
      FOR J IN 1..Discoteca.Bandas(I).Info_Albumes.Cuantos_Albumes  LOOP

         IF Discoteca.Bandas(I).Info_Albumes.Albumes(J).Anio >= Inicio AND Discoteca.Bandas(I).Info_Albumes.Albumes(J).Anio <= Fin THEN
            Cont := Cont+1;
         END IF;
      END LOOP;
      --  put_line(Integer'Image(cont));
      IF Cont > Mayor THEN
         Mayor := Cont;
         Banda := Discoteca.Bandas(I).Nombre_Banda;
      END IF;

   END LOOP;
   RETURN Banda;
END Banda_Mas_Prolifica;
