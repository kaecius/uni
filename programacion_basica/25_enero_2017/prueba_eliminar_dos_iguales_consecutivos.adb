with datos;use datos;
with Ada.Text_IO; use Ada.Text_IO;
procedure prueba_eliminar_dos_iguales_consecutivos is
   
   procedure insertar_al_comienzo (
                                   L   : in out Lista;  
                                   Num : in     Integer ) is 
      -- pre:
      -- post: Se ha insertado Num al comienzo de L
   
      Nuevo : Lista;  
   begin
      Nuevo := new Nodo;
      Nuevo.Valor := num;
      Nuevo.Sig := L;
      L := Nuevo;

   end insertar_al_comienzo;

   procedure escribir(lis : Lista) is
      l_tmp : Lista;
   begin
      l_tmp:= lis;
      Put("<");
      while l_tmp /= null loop
         Put(" " & Integer'Image(l_tmp.Valor));
         l_tmp := l_tmp.Sig;
      end loop;
      Put(">");      
   end escribir;
     
   
   procedure eliminar_dos_iguales_consecutivos(L : in out Lista) is
      l_anterior : Lista;
      l_actual : Lista;
      l_tmp : Lista;
   begin
      l_anterior := null;
      l_actual := L;
      if l_actual /= null then
         
         while l_actual /= null AND THEN l_actual.Sig /= null loop
            if l_actual.Valor = l_actual.Sig.Valor then
               l_tmp := l_actual.Sig;
               if l_anterior /= null then
                  l_anterior.Sig := l_tmp.Sig;
                  l_actual := l_anterior;
               else
                  L := l_tmp.Sig;
                  l_actual := L;
               end if;
            end if;
            l_anterior := l_actual;
            if l_actual /= null then
               l_actual := l_actual.Sig;
            end if; 
         end loop;
      
      end if;
   end eliminar_dos_iguales_consecutivos;
   
   lis : Lista := null;
   
begin
   put_line("********************");
   put_line("Programa de prueba: ");
   put_line("********************");

   lis := null;
   insertar_al_comienzo(Lis, 4);
   insertar_al_comienzo(Lis, 4);
   insertar_al_comienzo(Lis, 5);
   insertar_al_comienzo(Lis, 6);
   insertar_al_comienzo(Lis, 6);
   insertar_al_comienzo(Lis, 6);
   insertar_al_comienzo(Lis, 3);
   escribir(Lis);
   new_line;
   new_line;
   Eliminar_Dos_Iguales_Consecutivos(Lis);
   escribir(Lis);
   new_line;
   put_line("********************");
   
   lis := null;
   insertar_al_comienzo(Lis, 4);
   insertar_al_comienzo(Lis, 4);

   escribir(Lis);
   new_line;
   new_line;
   Eliminar_Dos_Iguales_Consecutivos(Lis);
   escribir(Lis);
   new_line;

   put_line("********************");   
   lis := null;
   insertar_al_comienzo(Lis, 4);
   insertar_al_comienzo(Lis, 4);
   insertar_al_comienzo(Lis, 5);
   insertar_al_comienzo(Lis, 5);
   insertar_al_comienzo(Lis, 6);
   escribir(Lis);
   new_line;
   new_line;
   Eliminar_Dos_Iguales_Consecutivos(Lis);
   escribir(Lis);
   new_line;

   put_line("********************");
   lis := null;
   insertar_al_comienzo(Lis, 4);
   insertar_al_comienzo(Lis, 4);
   insertar_al_comienzo(Lis, 5);
   insertar_al_comienzo(Lis, 6);
   insertar_al_comienzo(Lis, 6);
   insertar_al_comienzo(Lis, 6);
   insertar_al_comienzo(Lis, 3);
   escribir(Lis);
   new_line;
   new_line;
   Eliminar_Dos_Iguales_Consecutivos(Lis);
   escribir(Lis);
   new_line;

   put_line("********************"); 
   lis := null;
   insertar_al_comienzo(Lis, 4);
   insertar_al_comienzo(Lis, 5);
   insertar_al_comienzo(Lis, 5);
   insertar_al_comienzo(Lis, 6);
   insertar_al_comienzo(Lis, 5);
   insertar_al_comienzo(Lis, 7);
   insertar_al_comienzo(Lis, 7);
   escribir(Lis);
   new_line;
   new_line;
   Eliminar_Dos_Iguales_Consecutivos(Lis);
   escribir(Lis);
   new_line;
   put_line("********************");
   lis := null;
   insertar_al_comienzo(Lis, 4);
   escribir(Lis);
   new_line;
   new_line;
   Eliminar_Dos_Iguales_Consecutivos(Lis);
   escribir(Lis);
   new_line;
   put_line("********************");
   lis := null;
 
   escribir(Lis);
   new_line;
   new_line;
   Eliminar_Dos_Iguales_Consecutivos(Lis);
   escribir(Lis);
   new_line;
   put_line("********************");
   
   lis := null;
   insertar_al_comienzo(Lis, 4);
   insertar_al_comienzo(Lis, 5);
   escribir(Lis);
   new_line;
   new_line;
   Eliminar_Dos_Iguales_Consecutivos(Lis);
   escribir(Lis);
   new_line;
   
end prueba_eliminar_dos_iguales_consecutivos;