package datos is
   Max_pasajeros : constant Integer := 200;
   type info_pasajero is record
      Nombre : String (1..60);
      DNI : String(1..9);
      Codigo_grupo : String(1..6);
      Preferente : boolean;
      Num_fila_asiento : Positive;
      Letra_Asiento : Character;
   end record;

   type Vector_pasajeros is array (1..Max_pasajeros) of info_pasajero;
   type cola_pasajeros is record
      cuantos : Integer;
      pasajeros : Vector_pasajeros;
   end record;

   type grupo_pasajeros is record
      codigo_grupo : String(1..6);
      grupo_pasajeros :cola_pasajeros;
   end record;

   type vector_grupo_pasajeros is array (1..Max_pasajeros) of grupo_pasajeros;

   type cola_grupos_pasajeros is record
      num_grupos : Integer;
      grupos_pasajeros : vector_grupo_pasajeros;
   end record;



end datos;
