with datos;use datos;
with Ada.Text_IO; use Ada.Text_IO;
procedure grupos_pasajeros is
   function esta_grupo(codigo_grupo : String ; cola : cola_grupos_pasajeros) return Integer is
      result : Integer := -1;
      pos : integer := 1;
   begin

      while pos <= cola.num_grupos AND THEN cola.grupos_pasajeros(pos).codigo_grupo /= codigo_grupo loop
         pos := pos+1;
      end loop;
      if pos <= cola.num_grupos then
         result := pos;
      end if;
      return result;
   end esta_grupo;
   procedure anadir_a_cola(pasajero_actual : info_pasajero; cola : in out cola_grupos_pasajeros) is
      tmp_grupo : grupo_pasajeros ;
   begin
      tmp_grupo.codigo_grupo := pasajero_actual.Codigo_grupo;
      tmp_grupo.grupo_pasajeros.cuantos := 1;
      tmp_grupo.grupo_pasajeros.pasajeros(1) := pasajero_actual;

      cola.num_grupos := cola.num_grupos +1;
      cola.grupos_pasajeros(cola.num_grupos).codigo_grupo := pasajero_actual.Codigo_grupo;
      cola.grupos_pasajeros(cola.num_grupos) := tmp_grupo;

   end anadir_a_cola;
   procedure anadir_pasajero_a_grupo(pasajero_actual : info_pasajero; pos_grupo : Integer ; cola: in out cola_grupos_pasajeros)is
   begin
      cola.grupos_pasajeros(pos_grupo).grupo_pasajeros.cuantos := cola.grupos_pasajeros(pos_grupo).grupo_pasajeros.cuantos +1;
      cola.grupos_pasajeros(pos_grupo).grupo_pasajeros.pasajeros(cola.grupos_pasajeros(pos_grupo).grupo_pasajeros.cuantos) := pasajero_actual;
   end anadir_pasajero_a_grupo;
   procedure mover_grupo(pos_grupo : Integer; cola_actual : in out cola_grupos_pasajeros; cola_final : in out cola_grupos_pasajeros)is
      tmp_grupo : grupo_pasajeros;
   begin
      -- Supongo que solo se mantiene el orden de llegada de la cola actual
      tmp_grupo := cola_actual.grupos_pasajeros(pos_grupo);
      for i in pos_grupo+1..cola_actual.num_grupos loop
         cola_actual.grupos_pasajeros(i-1) := cola_actual.grupos_pasajeros(i);
      end loop;
      cola_actual.num_grupos := cola_actual.num_grupos-1;
      cola_final.num_grupos := cola_final.num_grupos +1;
      cola_final.grupos_pasajeros(cola_final.num_grupos) := tmp_grupo;

   end mover_grupo;

   procedure getColasPreferentesNoPreferentes(pasajeros : in cola_pasajeros; preferentes : out cola_grupos_pasajeros; noPreferentes : out cola_grupos_pasajeros) is
      pos_grupo : Integer;
      pasajero_actual : info_pasajero;
   begin
      preferentes.num_grupos := 0;
      noPreferentes.num_grupos := 0;
      for i in 1..pasajeros.cuantos loop
         pasajero_actual := pasajeros.pasajeros(i);

         pos_grupo := esta_grupo(pasajero_actual.Codigo_grupo,preferentes);

         if pos_grupo /= -1  then
            -- El grupo existe y esta en preferentes
            anadir_pasajero_a_grupo(pasajero_actual,pos_grupo,preferentes);
         else
            pos_grupo := esta_grupo(pasajero_actual.Codigo_grupo,noPreferentes);

            if pos_grupo /= -1 then
               -- El grupo existe y esta en no preferentes
               if pasajero_actual.Preferente then
                  --Añadir a grupo y mover a preferentes
                  anadir_pasajero_a_grupo(pasajero_actual,pos_grupo,noPreferentes);
                  mover_grupo(pos_grupo,noPreferentes,preferentes);
               else
                  --Añadir al grupo
                  anadir_pasajero_a_grupo(pasajero_actual,pos_grupo,noPreferentes);
               end if;
            else
               --El grupo no existe
               if pasajero_actual.Preferente then
                  -- Añadir a los preferentes
                  anadir_a_cola(pasajero_actual,preferentes);
               else
                  anadir_a_cola(pasajero_actual,noPreferentes);
               end if;
            end if;

         end if;

      end loop;

   end getColasPreferentesNoPreferentes;



   pasajeros : cola_pasajeros;
   cola_preferente : cola_grupos_pasajeros;
   cola_no_preferente : cola_grupos_pasajeros;
begin
   pasajeros.cuantos := 7;
   pasajeros.pasajeros(1).Codigo_grupo := "000001";
   pasajeros.pasajeros(1).DNI := "000000123";
   pasajeros.pasajeros(1).Preferente := False;

   pasajeros.pasajeros(2).Codigo_grupo := "000001";
   pasajeros.pasajeros(2).DNI := "000000345";
   pasajeros.pasajeros(2).Preferente := False;

   pasajeros.pasajeros(3).Codigo_grupo := "000002";
   pasajeros.pasajeros(3).DNI := "000000374";
   pasajeros.pasajeros(3).Preferente := False;

   pasajeros.pasajeros(4).Codigo_grupo := "000003";
   pasajeros.pasajeros(4).DNI := "000009876";
   pasajeros.pasajeros(4).Preferente := False;

   pasajeros.pasajeros(5).Codigo_grupo := "000002";
   pasajeros.pasajeros(5).DNI := "000000346";
   pasajeros.pasajeros(5).Preferente := true;

   pasajeros.pasajeros(6).Codigo_grupo := "000003";
   pasajeros.pasajeros(6).DNI := "000000342";
   pasajeros.pasajeros(6).Preferente := False;

   pasajeros.pasajeros(7).Codigo_grupo := "000001";
   pasajeros.pasajeros(7).DNI := "000004234";
   pasajeros.pasajeros(7).Preferente := False;

   getColasPreferentesNoPreferentes(pasajeros,cola_preferente,cola_no_preferente);
   Put_Line("Preferentes");
   for i in 1..cola_preferente.num_grupos loop
      Put_Line("    ID Grupo: " & cola_preferente.grupos_pasajeros(i).codigo_grupo);
      for j in 1.. cola_preferente.grupos_pasajeros(i).grupo_pasajeros.cuantos loop
         Put_Line("        " & cola_preferente.grupos_pasajeros(i).grupo_pasajeros.pasajeros(j).DNI);
      end loop;
      Put_Line("-------");
   end loop;
   Put_Line("No Preferentes");
   for i in 1..cola_no_preferente.num_grupos loop
      Put_Line("    ID Grupo: " & cola_no_preferente.grupos_pasajeros(i).codigo_grupo);
      for j in 1.. cola_no_preferente.grupos_pasajeros(i).grupo_pasajeros.cuantos loop
         Put_Line("        " & cola_no_preferente.grupos_pasajeros(i).grupo_pasajeros.pasajeros(j).DNI);
      end loop;
      Put_Line("-------");
   end loop;


end grupos_pasajeros;